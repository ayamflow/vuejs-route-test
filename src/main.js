var page = require('page'),
    Vue = require('vue'),
    homeSection = require('./sections/home/home'),
    aboutSection = require('./sections/about/about');

var routes = {
    home: false,
    about: false
};

function home() {
    // Fetch data & stuff
}

function about() {

}

function update(context, next) {
    var current = context.path.substr(1) || 'home';
    for(var route in routes) {
        routes[route] = (route == current);
    }
    console.log(routes);
    next();
}

function notfound() {
    page('/');
}

var root = new Vue({
    el: '#view',
    data: {
        routes: routes
    }
});

page('/*', update);
page('/', home);
page('/about', about);
page('*', notfound);
page.start();