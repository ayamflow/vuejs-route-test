var Vue = require('vue'),
    template = require('./home.html');

exports = Vue.component('home-section', {
    template: template,
    data: {
        "title": "Vue.js",
        "subtitle": "MVVM made simple",
        "description1": "Vue.js is a library for building interactive web interfaces.",
        "description2": "It provides efficient MVVM data bindings with a simple and flexible API."
    },
    methods : {

    }
});