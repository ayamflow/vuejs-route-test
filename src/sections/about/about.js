var Vue = require('vue'),
    template = require('./about.html');

exports = Vue.component('about-section', {
    template: template,
    data: {
        "title": "Class: Vue",
        "description": "The Vue Class is the core of vue.js. It is a constructor that allows you to create ViewModel instances. Creating a ViewModel instance is straightforward:"
    },
    methods : {

    }
});