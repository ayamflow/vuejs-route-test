var gulp = require('gulp'),
    connect = require('gulp-connect'),
    component = require('gulp-component');

gulp.task('component', function() {
    gulp.src('./component.json')
    .pipe(component({
        standalone: true
    }))
    .pipe(gulp.dest('./build'));
});

gulp.task('connect', connect.server({
  root: __dirname,
  port: ~~(Math.random()) * 10000,
  open: {
    file: 'index.html',
    browser: 'Google Chrome'
  }
}));

gulp.task('watch', function() {
    gulp.watch('src/**/*.js', ['component']);
    gulp.watch('src/**/*.html', ['component']);
});